import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzer {
    private int benutzernummer;
    private String vorname;
    private String name;
    private int geburtsjahr;

    public Benutzer() {

    }

    public Benutzer(int benutzernummer, String vorname, String name, int geburtsjahr) {
        this.benutzernummer = benutzernummer;
        this.vorname = vorname;
        this.name = name;
        this.geburtsjahr = geburtsjahr;
    }

    public void setBenutzernummer() {
        this.benutzernummer = benutzernummer;
    }

    public void serVorname() {
        this.vorname = vorname;
    }

    public void setName() {
        this.name = name;
    }

    public void setGeburtsjahr() {
        this.geburtsjahr = geburtsjahr;
    }

    public int getBenutzernummer(int benutzernummer) {
        return benutzernummer;
    }

    public String getVorname(String vorname) {
        return vorname;
    }

    public String getName(String name) {
        return name;
    }

    public int getGeburtsjahr(int geburtsjahr) {
        return geburtsjahr;
    }

    public int getAlter(int geburtsjahr) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        Date date = new Date();
        int aktuellesJahr = Integer.parseInt(formatter.format(date));

        int alter = aktuellesJahr - geburtsjahr;

        return alter;
    }

    
    @Override
    public String toString() {
        return "Benutzer-ID: " + this.benutzernummer + " | Vorname: " + this.vorname + " | Nachname: " + this.name
                + " | Geburtsjahr " + this.geburtsjahr + " | Alter: " + getAlter(geburtsjahr);
    }

}
