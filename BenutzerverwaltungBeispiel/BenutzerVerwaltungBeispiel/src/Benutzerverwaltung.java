import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {

	public static void main(String[] args) {

		ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
		int auswahl;

		do {
			auswahl = menu();
			switch (auswahl) {
				case 1:
					benutzerAnzeigen(benutzerliste);
					break;
				case 2:
					benutzerErfassen(benutzerliste);
					break;
				case 3:
					// benutzerLoeschen();
					break;
				case 4:
					System.exit(0);
				default:
					System.err.println("\nFalsche Eingabe.\n");
			}
		} while (true);

	}

	public static int menu() {

		int selection;
		Scanner input = new Scanner(System.in);

		/***************************************************/

		System.out.println("     Benutzerverwaltung     ");
		System.out.println("----------------------------\n");
		System.out.println("1 - Benutzer anzeigen");
		System.out.println("2 - Benutzer erfassen");
		System.out.println("3 - Benutzer loeschen");
		System.out.println("4 - Ende");

		System.out.print("\nEingabe:  ");
		selection = input.nextInt();
		return selection;
	}

	public static void benutzerAnzeigen(ArrayList<Benutzer> benutzerliste) {

		for (int i = 0; i < benutzerliste.size(); i++) {
			System.out.println(benutzerliste.get(i));
		}

	}

	public static void benutzerErfassen(ArrayList<Benutzer> benutzerliste) {
		Scanner tastatur = new Scanner(System.in);
		int benutzernummer = 0;
		String vorname = "";
		String name = "";
		int geburtsjahr = 0000;

		// Eingabe Benutzerdetails
		System.out.println("Bitte geben Sie die Benutzer-ID ein: \n");
		benutzernummer = tastatur.nextInt();
		System.out.println("Bitte geben Sie den Vornamen ein: \n");
		vorname = tastatur.next();
		System.out.println("Bitte geben Sie den Nachnamen ein: \n");
		name = tastatur.next();
		System.out.println("Geben Sie das Geburtsjahr an: \n");
		geburtsjahr = tastatur.nextInt();
		// Hinzufügen der Benutzerdetails in einen Benutzer -> in die Arraylist
		Benutzer b1 = new Benutzer(benutzernummer, vorname, name, geburtsjahr);
		benutzerliste.add(b1);
	}

}