import java.util.ArrayList;

public class ArrayListBeispiele {
    public static void main(String[] args) throws Exception {

        // Arrayliste namenliste
        ArrayList<String> namenliste = new ArrayList<String>();

        namenliste.add("Max");
        namenliste.add("Alex");
        namenliste.add("Anna");

        System.out.println(namenliste);

        namenliste.remove(1);

        System.out.println(namenliste);

        // ArrayListe <Klasse> "Buchliste"
        ArrayList<Buch> buchliste = new ArrayList<Buch>();
        Buch b1 = new Buch("OOP", 20.99);

        buchliste.add(b1);

        System.out.println(buchliste);

        //==================================
        //IMPLEMENTIERUNG PERSON-ARRAYLISTE
        //==================================

        //Arrayliste personenliste
        ArrayList<Person> personenliste = new ArrayList<Person>();

        //Erstellen eines Objektes für die Arrayliste
        Person p1 = new Person("Tran", 21);

        //Hinzufügen von Einträgen
        personenliste.add(p1);

        //Ausgabe zur Kontrolle
        System.out.println(personenliste);

    }
}
